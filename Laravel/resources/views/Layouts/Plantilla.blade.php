<link href = "{{asset('css/bootstrap.min.css')}}" rel = "stylesheet">
<script src = "js/bootstrap.min.js"></script>

@yield("Cabecera")
@include("Layouts.NavBar")

@yield("Cuerpo")
@include("Layouts.Content")

@yield("Pie")
@include("Layouts.Footer")