<?php
    use App\Marca;
    $Brands = Marca::all();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href = "{{asset('css/bootstrap.min.css')}}" rel = "stylesheet">
    <link href = "{{asset('css/all.min.css')}}" rel = "stylesheet">
    <title>Modificar</title>
</head>
<body class = "bg-warning">
<div class="container">
<h1 class = "text-center text-justify text-dark mt-5 mb-5">Modificar datos</h1>
<div class = "dropdown-divider"></div>
<form method = "post" action = "/Layouts2/{{ $marcas->id }}" class = "form-horizontal" role = "form" name = "Form" id = "Form">
<div class="form-group">
    <label class = "bg-secondary font-weight-bold">Marca: </label>
    <select name = "marca" class = "form-control font-weight-bold">
    <option value = "Opciones" class = "bg-dark font-weight-bold text-white" disabled>Seleccionar la marca...</option>
    @foreach($Brands as $marca)
        @if($marca->id == $marcas->id)
        <option value = "{{ $marca->id }}" selected = "selected" class = "font-weight-bold">{{ $marca->marca }}</option>
        @else
        <option value = "{{ $marca->id }}" class = "font-weight-bold">{{ $marca->marca }}</option>
        @endif
    @endforeach
    </select>
</div>
<input type = "hidden" name = "_method" value = "PUT">
<input class="btn btn-success" type="submit" name="button" id="button" value="Guardar">
{{ csrf_field() }}
</form>
</div>
</body>
</html>