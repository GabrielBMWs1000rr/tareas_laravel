<div class="container">
<h1 class = "text-center text-justify text-dark mt-5 mb-5">Insertar datos de nuevo celular</h1>
<div class = "dropdown-divider"></div>
    <br>
    <form action = "{{ route('storephones') }}" method = "POST" class = "form-horizontal" role = "form" name = "Form" id = "Form">
    <div class="form-group">
        <label class = "bg-secondary font-weight-bold">Modelo: </label>
        <input type = "text" name = "model" id = "model" class = "form-control font-weight-bold" placeholder = "Ingrese el modelo del celular...">
    </div>
    <div class="form-group">
        <label class = "bg-secondary font-weight-bold">Año: </label>
        <input type = "number" name = "year" id = "year" class = "form-control font-weight-bold" placeholder = "Ingrese el año del celular...">
    </div>
    <div class="form-group">
        <label class = "bg-secondary font-weight-bold">Precio: </label>
        <input type = "number" name = "price" id = "price" class = "form-control font-weight-bold" placeholder = "Ingrese el precio del celular...">
    </div>
    <div class="form-group">
        <label class = "bg-secondary font-weight-bold">Marca: </label>
        <select name = "marca_id" class = "form-control font-weight-bold">
        <option value = "Opciones" class = "bg-dark font-weight-bold text-white">Seleccionar la marca...</option>
        @foreach($Brands as $marca)
        <option value = "{{ $marca->id }}" class = "font-weight-bold">{{ $marca->marca }}</option>
        @endforeach
        </select>
    </div>
    <button type = "submit" class = "btn btn-success">Agregar datos</button>
    {{ csrf_field() }}
    </form>
</div>