<div class="container">
<h1 class = "text-center text-justify text-dark mt-5 mb-5">Agregar marca</h1>
<div class = "dropdown-divider"></div>
    <br>
    <form method = "POST" action = "{{ route('storebrands') }}" class = "form-horizontal" role = "form" name = "Form" id = "Form" >
    <div class="form-group">
        <label class = "bg-secondary font-weight-bold">Marca: </label>
        <input type = "text" name = "brand" id = "brand" class = "form-control font-weight-bold" placeholder = "Ingrese la nueva marca...">
    </div>
    <button type = "submit" class = "btn btn-success">Agregar datos</button>
    {{ csrf_field() }}
    </form>
</div>