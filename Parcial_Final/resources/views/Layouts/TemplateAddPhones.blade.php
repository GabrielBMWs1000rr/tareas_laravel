<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href = "{{asset('css/bootstrap.min.css')}}" rel = "stylesheet">
    <link href = "{{asset('css/all.min.css')}}" rel = "stylesheet">
    <title>Agregar celulares</title>
</head>
<body class = "bg-warning">
@yield('Contenido')
@include('Layouts.TableAddPhones')
    <script src = "{{ asset('js/bootstrap.min.js')}}"></script>
</body>
</html>