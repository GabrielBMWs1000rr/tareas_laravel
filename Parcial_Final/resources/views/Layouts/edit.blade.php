<?php
    use App\Marca;
    use App\Celulare;
    $Brands = Marca::all();
    $Phones = Celulare::all();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href = "{{asset('css/bootstrap.min.css')}}" rel = "stylesheet">
    <link href = "{{asset('css/all.min.css')}}" rel = "stylesheet">
    <title>Modificar</title>
</head>
<body class = "bg-warning">
<div class="container">
<h1 class = "text-center text-justify text-dark mt-5 mb-5">Modificar datos</h1>
<div class = "dropdown-divider"></div>
    <br>
    <form method = "post" action = "/Layouts/{{ $Celulares->id }}" class = "form-horizontal" role = "form" name = "Form" id = "Form">
    <div class="form-group">
        <label class = "bg-secondary font-weight-bold">Id: </label>
        <input type = "number" name = "id" id = "id" class = "form-control font-weight-bold" value = "{{$Celulares->id}}" disabled>
    </div>
    <div class="form-group">
        <label class = "bg-secondary font-weight-bold">Modelo: </label>
        <input type = "text" name = "modelo" id = "modelo" class = "form-control font-weight-bold" value = "{{$Celulares->modelo}}">
    </div>
    <div class="form-group">
        <label class = "bg-secondary font-weight-bold">Año: </label>
        <input type = "number" name = "anio" id = "anio" class = "form-control font-weight-bold" value = "{{$Celulares->anio}}">
    </div>
    <div class="form-group">
        <label class = "bg-secondary font-weight-bold">Precio: </label>
        <input type = "number" name = "precio" id = "precio" class = "form-control font-weight-bold" value = "{{$Celulares->precio}}">
    </div>
    <div class="form-group">
        <label class = "bg-secondary font-weight-bold">Marca: </label>
        <select name = "marca_id" class = "form-control font-weight-bold">
        <option value = "Opciones" class = "bg-dark font-weight-bold text-white" disabled>Seleccionar la marca...</option>
        @foreach($Brands as $marca)
           @if($marca->id == $Celulares->marca_id)
            <option value = "{{ $marca->id }}" selected = "selected" class = "font-weight-bold">{{ $marca->marca }}</option>
           @else
            <option value = "{{ $marca->id }}" class = "font-weight-bold">{{ $marca->marca }}</option>
            @endif
        @endforeach
        </select>
    </div>
    <input type = "hidden" name = "_method" value = "PUT">
    <input class="btn btn-success" type="submit" name="button" id="button" value="Guardar">
    {{ csrf_field() }}
    </form>
</div>
    <script src = "{{ asset('js/bootstrap.min.js')}}"></script>
</body>
</html>