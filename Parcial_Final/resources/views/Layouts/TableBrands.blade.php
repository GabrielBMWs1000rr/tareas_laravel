<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <a class="navbar-brand" href="#">Phones</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link font-weight-bold" href="/Telefonos">Table 1</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link font-weight-bold" href="/Marcas">Table 2</a>
      </li>
    </ul>
  </div>
</nav>
<div class = "container">
<h1 class = "text-center text-justify text-dark mt-5 mb-3">Marcas</h1>
<div class = "dropdown-divider"></div>
    <table class = "table table-bordered table-dark text-center mt-5">
    <tr><td><h2 class="text-center text-white font-weight-bold">Id</h2></td>
    <td><h2 class="text-center text-white font-weight-bold">Marca</h2></td>
    <td><h2 class = 'text-primary font-weight-bold'>Editar</h2></td>
    <td><h2 class = 'text-danger font-weight-bold'>Borrar</h2></td></tr>
    @foreach($Brands as $marca)
    <tr><td>{{ $marca->id}}</td>
    <td>{{ $marca->marca }}</td>
    <td><a href = "{{ route('Layouts2.edit', $marca->id) }}"><button class = "btn btn-primary w-50"><i class = "fas fa-edit"></i></button></td>
    <td>
    <form method = "post" action = "{{ url('/Layouts2/'.$marca->id) }}">
      {{ csrf_field() }}
      {{ method_field('DELETE') }}
    <button class = "btn btn-danger w-50"><i class = "fas fa-trash"></i></button>
    </form>
    </td></tr>
    @endforeach
    </table>
<div class = "d-flex justify-content-center">
    <button type = "submit" class = "btn btn-success"><a href = "{{ route('createbrands') }}" class = "text-white">Agregar marca</a></button>
</div>
</div>