<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//------------------------------------------//
//Rutas para trabajar con la tabla celulares//
//------------------------------------------//

Route::get('/Telefonos', 'PhonesController@index')->name('viewphones');
Route::get('/Agregar_Telefonos', 'PhonesController@create')->name('createphones');
Route::post('/Agregar_Telefonos', 'PhonesController@store')->name('storephones');
Route::resource('/Layouts', 'PhonesController'); 

//---------------------------------------//
//Rutas para trabajar con la tabla marcas//
//---------------------------------------//

Route::get('/Marcas', 'BrandsController@index')->name('viewbrands');
Route::get('/Agregar_Marcas', 'BrandsController@create')->name('createbrands');
Route::post('/Agregar_Marcas', 'BrandsController@store')->name('storebrands');
Route::resource('/Layouts2', 'BrandsController');