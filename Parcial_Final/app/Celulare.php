<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Marca;

class Celulare extends Model
{
    protected $fillable = ['modelo', 'anio', 'precio', 'marca_id'];

    public function Marcas()
    {
        return $this->belongsTo(Marca::class);
    }
}