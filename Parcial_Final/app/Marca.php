<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Celulare;

class Marca extends Model
{
    protected $fillable = ['marca'];

    public function Celulares()
    {
        return $this->hasMany(Celulare::class);
    }
}