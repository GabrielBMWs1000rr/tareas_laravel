<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Celulare;
use App\Marca;

class PhonesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Phones = Celulare::all();
        $Brands = Marca::all();

        return view('Telefonos', compact(['Phones', 'Brands']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Brands = Marca::all();

        return view('Agregar_Telefonos', compact('Brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Celulares = new Celulare();

        $Celulares->modelo = $request->input('model');
        $Celulares->anio = $request->input('year');
        $Celulares->precio = $request->input('price');
        $Celulares->marca_id = $request->input('marca_id');
        $Celulares->save();

        return redirect()->route('viewphones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Celulares = Celulare::findOrFail($id);
        return view('Layouts.show', compact('Celulares'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Celulares = Celulare::findOrFail($id);
        return view('Layouts.edit', compact('Celulares'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Celulares = Celulare::findOrFail($id);
        $Celulares->modelo = $request->input('modelo');
        $Celulares->anio = $request->input('anio');
        $Celulares->precio = $request->input('precio');
        $Celulares->marca_id = $request->input('marca_id');
        $Celulares->save();
        return redirect("/Telefonos");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Celulare::destroy($id);

       return redirect('Telefonos');
    }
}