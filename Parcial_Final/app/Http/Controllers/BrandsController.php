<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Celulare;
use App\Marca;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Brands = Marca::all();

        return view('Marcas', compact('Brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Brands = Marca::all();

        return view('Agregar_Marcas', compact('Brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brands = new Marca();

        $brands->marca = $request->input("brand");
        $brands->save();

        return redirect()->route('viewbrands');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $marcas = Marca::findOrFail($id);
        return view('Layouts2.show', compact('marcas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marcas = Marca::findOrFail($id);
        return view('Layouts2.edit', compact('marcas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Marcas = Marca::findOrFail($id);
        $Marcas->marca = $request->input('marca');
        $Marcas->save();
        return redirect('/Marcas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Marca::destroy($id);
        return redirect('Marcas');
    }
}