<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("/noticia", "NoticiaController@Noticia");

Route::get("/Users", "NoticiaController@Usuarios");

Route::get("/Educación", "NoticiaController@Kids");

Route::get("/Deportes", "NoticiaController@Sports");

Route::get("/Cultura", "NoticiaController@Culture");

Route::get("/Users", "NoticiaController@Users");

Route::resource("/layouts", "UsuariosController");