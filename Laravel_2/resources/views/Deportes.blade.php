<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href = "{{asset('css/bootstrap.min.css')}}" rel = "stylesheet">
    <script src = "js/bootstrap.min.js"></script>
    <style>
    .Message {
      display: flex;
      justify-content: center;
      margin-top: 15%;
    }
    </style>
    <title>Deportes</title>
</head>
<body class = "bg-warning">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="background-color: #e3f2fd;">
  <a class="navbar-brand text-primary" href="#">SN Noticias</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/noticia">Locales</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/Educación">Educación</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/Deportes">Deportes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/Cultura">Cultura</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/Users">Usuarios</a>
      </li>
    </ul>
  </div>
</nav>
<div class = "Message">
    <h3 class = "text-dark text-center mt-5 font-weight-bold">No hay nada para mostrar aquí...</h3>
</div>
</body>
</html>