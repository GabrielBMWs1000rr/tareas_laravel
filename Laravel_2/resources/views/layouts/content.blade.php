<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class = "bg-warning">
    <div class = "container">
    <h1 class = "text-dark mt-5 mb-5 text-justify">Noticias locales <span class="badge badge-secondary"> 20</span></h1>
    <div class = "dropdown-divider mb-5"></div>
    <h3 class = "text-danger ">Noticia 1</h3>
    <p class = "text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?</p>
    <div class = "dropdown-divider mb-5"></div>
    <h3 class = "text-danger ">Noticia 2</h3>
    <p class = "text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?</p>
    <div class = "dropdown-divider mb-5"></div>
    <h3 class = "text-danger ">Noticia 3</h3>
    <p class = "text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?</p>
    <div class = "dropdown-divider mb-5"></div>
    <h3 class = "text-danger ">Noticia 4</h3>
    <p class = "text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?</p>
    <div class = "dropdown-divider mb-5"></div>
    <h3 class = "text-danger ">Noticia 5</h3>
    <p class = "text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente debitis eveniet voluptas 
    veritatis quas voluptates dolor doloribus odit consequuntur? Perferendis eum cupiditate numquam 
    totam quisquam nemo suscipit dolor quod iusto?</p>
    </div>
</body>
</html>