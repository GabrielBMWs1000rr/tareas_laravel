<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href = "{{asset('css/bootstrap.min.css')}}" rel = "stylesheet">
    <script src = "js/bootstrap.min.js"></script>
    <title>Noticias</title>
</head>
<body class = "bg-light">

@include("layouts.navbar")
@yield("Cabecera")
<div class="container">
    <br>
    @foreach ($noticias as $noticia)
    
        @if($loop->first)
            <h1 class='text-success'>Noticias Locales <span class='badge badge-secondary'>{{$loop->count}}</span></h1>  
            <div class="card-group">
        @endif

        @include("layouts.card")
        @if($loop->iteration % 4 == 0)
            </div>
            <div class="card-group">
        @endif

        @if($loop->last)
        </div>
        @endif

    @endforeach
    
</div> 

@yield("pie")
@include("layouts.footer")

</body>
</html>