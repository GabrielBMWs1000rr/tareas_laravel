<div style = 'border-width: 10px;' class="card bg-secondary border-dark">
    <img src="{{$noticia->imagen}}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title font-weight-bold">{{$noticia->titulo}}</h5>
      <p class="card-text text-justify font-weight-bold text-dark">{{$noticia->texto}}</p>
    </div>
    <div class="card-footer">
      <small class="text-white font-weight-bold">{{$noticia->updated_at->format('d-m-y')}}</small>
    </div>
</div>