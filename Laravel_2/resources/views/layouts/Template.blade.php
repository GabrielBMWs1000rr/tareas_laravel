<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href = "/favicon.ico">
  <link href = "{{asset('css/bootstrap.min.css')}}" rel = "stylesheet">
  <link href = "{{asset('css/all.min.css')}}" rel = "stylesheet">
  <style>

/*---------------------------------------------*/
/*Código Css del mensaje emergente para agregar*/
/*---------------------------------------------*/

  .overlay {
    background-color: rgba(0, 0, 0, 0.7);
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    visibility: hidden;
  }

  .overlay.Active {
    visibility: visible;
  }

  .popup {
    background-color: rgba(255, 255, 255, 0.9);
    border-radius: 5px;
    padding-left: 30px;
    padding-right: 30px;
    padding-bottom: 30px;
    transition: .3s ease all;
    transform: scale(0.7);
    opacity: 0;
  }

  .popup.Active {
    opacity: 1;
    transform: scale(1);
  }

  .popup h2 {
    opacity: 0;
  }

  .popup h7 {
    opacity: 0;
  }

  .popup form .Animación {
    opacity: 0;
  }

  .popup.Active h2 {
    animation: EntradaTítulo 4s ease .5s forwards;
  }

  @keyframes EntradaTítulo {
    from {
      transform: translateY(-25px);
      opacity: 0;
    }
    to {
      transform: translateY(0);
      opacity: 1;
    }
  }

  .popup.Active form h7 {
    animation: EntradaSubtítulos 4s ease .5s forwards;
  }

  @keyframes EntradaSubtítulos {
    from {
      transform: translateX(-5px);
      opacity: 0;
    }
    to {
      transform: translateX(0);
      opacity: 1;
    }
  }

  .popup.Active form .Animación {
    animation: MoviendoInputs 1s ease 2s forwards; /*Primer tiempo es el tiempo que demora en 
                                                      ejecutarse la transición, y el segundo tiempo 
                                                      es el tiempo de espera para que empiece a 
                                                      ejecutarse la transición*/ 
  }

@keyframes MoviendoInputs {
  from {
    opacity :0;
  }
  to {
    opacity :1;
  }
}

  .bottom {
    margin-bottom: 10px;
  }

  .btn-cerrar-popup {
    font-size: 16px;
    margin-top: 10px;
    display: block;
    text-align: right;
    color: #636b6f;
    transition: 2s ease all;
  }

  .btn-cerrar-popup:hover {
    color: red;
  }

  .Title {
    margin-bottom: 20px;
  }

  .btn-submit {
    border: none;
    border-radius: 5px;
    transition: .3s ease all;
  }

/*-----------------------------------------------*/
/*Código Css del mensaje emergente para modificar*/
/*-----------------------------------------------*/

.OverLay {
    background-color: rgba(0, 0, 0, 0.7);
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    visibility: hidden;
  }

  .OverLay.active {
    visibility: visible;
  }

  .PopUp {
    background-color: rgba(255, 255, 255, 0.9);
    border-radius: 5px;
    padding-left: 30px;
    padding-right: 30px;
    padding-bottom: 30px;
    transition: .3s ease all;
    transform: scale(0.7);
    opacity: 0;
  }

  .PopUp.active {
    opacity: 1;
    transform: scale(1);
  }

  .PopUp h2 {
    opacity: 0;
  }

  .PopUp h7 {
    opacity: 0;
  }
                          
  .PopUp form .animación {
    opacity: 0;
  }

  .PopUp.active h2 {
    animation: entradatítulo 4s ease .5s forwards;
  }

  @keyframes entradatítulo {
    from {
      transform: translateY(-25px);
      opacity: 0;
    }
    to {
      transform: translateY(0);
      opacity: 1;
    }
  }

  .PopUp.active form h7 {
    animation: entradasubtítulos 4s ease .5s forwards;
  }

  @keyframes entradasubtítulos {
    from {
      transform: translateX(-5px);
      opacity: 0;
    }
    to {
      transform: translateX(0);
      opacity: 1;
    }
  }

  .PopUp.active form .animación {
    animation: moviendoinputs 1s ease 2s forwards; /*Primer tiempo es el tiempo que demora en 
                                                      ejecutarse la transición, y el segundo tiempo 
                                                      es el tiempo de espera para que empiece a 
                                                      ejecutarse la transición*/ 
  }

@keyframes moviendoinputs {
  from {
    opacity :0;
  }
  to {
    opacity :1;
  }
}

  .Bottom {
    margin-bottom: 10px;
  }

  .btnCerrarPopup {
    font-size: 16px;
    margin-top: 10px;
    display: block;
    text-align: right;
    color: #636b6f;
    transition: 2s ease all;
  }

  .btnCerrarPopup:hover {
    color: red;
  }

  .title {
    margin-bottom: 20px;
  }

  .Btn-Submit {
    border: none;
    border-radius: 5px;
    transition: .3s ease all;
  }

  </style>
  <title>Usuarios</title>
</head>
<body>
@include("layouts.Nav")
@yield("Content")
@include("layouts.Table")
@include("layouts.PopUp")
<script src = "js/Work.js"></script>
<script src = "{{ asset('js/bootstrap.min.js')}}"></script>
</body>
</html>