<div class = "container">
  <br>
  <h1 class = "text-success">Usuarios</h1>
  <div class = "dropdown-divider"></div>
  <br>
    <button type = "button" id = '+' class="btn btn-outline-success btn-lg">+</button>
    <table class = "table table-bordered table-dark text-center mt-5">
      <td><h2 class = 'text-white font-weight-bold'>Id</h2></td>
      <td><h2 class = 'text-white font-weight-bold'>Nombre</h2></td>
      <td><h2 class = 'text-white font-weight-bold'>Apellido</h2></td>
      <td><h2 class = 'text-white font-weight-bold'>E-mail</h2></td>
      <td><h2 class = 'text-primary font-weight-bold'>Editar</h2></td>
      <td><h2 class = 'text-danger font-weight-bold'>Borrar</h2></td>
      @foreach($DatosPersonales as $Datos)
        <tr><td>{{$Datos->id}}</td>
        <td>{{$Datos->nombre}}</td>
        <td>{{$Datos->apellido}}</td>
        <td>{{$Datos->email}}</td>
        <td>
        <button type="submit" onclick = "ModifyClient(OverLay, popup)" class="btn btn-primary btn-lg w-100 Modificar"><i class="fas fa-edit"></i></button>
        </td>
        <td>
        <form method = "post" action = "{{ url('/layouts/'.$Datos->id) }}"> 
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <button type="submit" class="btn btn-danger btn-lg w-100"><i class = "fas fa-trash"></i></button>
        </form>
        </td></tr>
      @endforeach
    </table>
</div>