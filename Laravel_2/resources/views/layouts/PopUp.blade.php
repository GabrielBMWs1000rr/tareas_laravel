<div class = "overlay" id = "Lay">
  <div class = "popup w-50" id = "Pop">
    <a href = "#" id = "btn-cerrar-popup" class = "btn-cerrar-popup"><i class = "fas fa-times"></i></a>
    <h2 class = "Title text-success">Nuevo usuario</h2>
    <form method = "post" action = "/layouts">
      <div class = "contenedor-inputs">
        <h7 class = "font-weight-bold">Nombre: </h7>
        <input type = "text" name = "nombre" class = "font-weight-bold w-100 bottom Animación" placeholder = "Ingrese su nombre...">
        {{ csrf_field() }}
        <h7 class = "font-weight-bold">Apellido: </h7>
        <input type = "text" name = "apellido"  class = "font-weight-bold w-100 bottom Animación" placeholder = "Ingrese su apellido...">
        <h7 class = "font-weight-bold">E-Mail: </h7>
        <input type="email" name = "email"  class = "font-weight-bold w-100 Animación" placeholder = "Ingrese su correo...">
      </div>
      <br></br>
      <input type = "submit" class = "btn-submit font-weight-bold w-100 bg-success" value = "Guardar">
    </form>
  </div>
</div>
<div class = "OverLay" id = "lay">
  <div class = "PopUp w-50" id = "pop">
    <a href = "#" id = "btn-Cerrar-PopUp" onclick = "Close(OverLay, popup)" class = "btnCerrarPopup"><i class = "fas fa-times"></i></a>
    <h2 class = "title text-success">Editar usuario</h2>
    <form method = "post" action = "/layouts">
      <div class = "Contenedor-Inputs">
        <h7 class = "font-weight-bold">Nombre: </h7>
        <input type = "text" class = "font-weight-bold w-100 Bottom animación" placeholder = "Ingrese su nombre..." value = "">
        <h7 class = "font-weight-bold">Apellido: </h7>
        <input type = "text" class = "font-weight-bold w-100 Bottom animación" placeholder = "Ingrese su apellido..."  value = "">
        <h7 class = "font-weight-bold">E-Mail: </h7>
        <input type = "text" class = "font-weight-bold w-100 animación" placeholder = "Ingrese su correo..."  value = "">
      </div>
      <br></br>
      <input type = "submit" class = "Btn-Submit font-weight-bold w-100 bg-success" value = "Modificar">
    </form>
  </div>
</div>