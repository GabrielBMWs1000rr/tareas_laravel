<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Noticia;
use Faker\Generator as Faker;

$factory->define(Noticia::class, function (Faker $faker) 
{
    $created = $faker->dateTimeBetween('-10 years');

    return [
        'titulo'=> $faker->sentence,
        'texto'=> $faker->text,
        'imagen'=> $faker->imageUrl,
        'created_at'=> $created,
        'updated_at'=> $faker->dateTimeBetween($created),
    ];
});