<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticia;
use App\Usuario;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class NoticiaController extends Controller
{
    public function Noticia()
    {
        $noticias = Noticia::orderBy('id', 'desc')->paginate(12);
        return view("noticia",  ['noticias' => $noticias]);
    }

    public function Usuarios()
    {
        return view('Users');
    }

    public function Kids()
    {
        return view('Educación');
    }

    public function Sports()
    {
        return view('Deportes');
    }

    public function Culture()
    {
        return view('Cultura');
    }

    public function Users()
    {
        $DatosPersonales = Usuario::all();
        return view('Users', ['DatosPersonales' => $DatosPersonales]);
    }
}